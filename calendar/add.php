<!doctype html>


<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body

<header>
  <h2>PROJECT MANAGEMENT</h2>
</header>

<div class="container">
  <main class="edit">
    <form action="insert.php" method="post">

      <div class="id">
        <h3>Name</h3>
        <input type="text" id="name" name="name" placeholder="Name">
      </div>

      <div class="description">
        <h3>Start</h3>
        <input type="text" id="start" name="start" placeholder="dd.mm.yyyy">
      </div>

      <div class="date">
        <h3>End</h3>
        <input type="text" id="end" name="end" placeholder="dd.mm.yyyy">
      </div>

      <div class="submit">
        <input  type="submit" value="Submit">
      </div>

    </form>
  </main>
</div>


<footer>
  <div class="small-12 large-12 columns">
    <div class="copyrights">
      <p>&copy; Copyright 2017 Jakov Ivankovic</p>
    </div>
  </div>
</footer>


<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/dist/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="js/app.js"></script>
</body>
