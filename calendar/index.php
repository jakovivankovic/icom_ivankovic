<!doctype html>

<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Calendar</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body

<header>
  <div class="title">
    <a href="login.php" class="log">LOG</a>
    <h2>PROJECT MANAGEMENT</h2>
  </div>
</header>



<div class="container">
  <main>
    <ul class="tasklist">
      <tr>
        <th>janvier</th>
        <th>fevrier</th>
        <th>mars</th>
        <th>avril</th>
        <th>mai</th>
        <th>juin</th>
        <th>juillet</th>
        <th>aout</th>
        <th>septembre</th>
        <th>octobre</th>
        <th>novembre</th>
        <th>decembre</th>
      </tr>



      <?php foreach ($tasks as $task): ?>
        <li>
          <span class="tasklist-item-name"><?php echo $task['name']?></span>
          <span class="tasklist-item-start"><?php echo $task['start'] ?></span>
          <span class="tasklist-item-end"><?php echo $task['end'] ?></span>
          <a href="delete.php?value=<?php echo $task['id'];?>" class="tasklist-item-delete">supprimer</a>
        </li>
      <?php endforeach; ?>
      <a href="add.php" class="btnAdd">add</a>
    </main>
  </div>



  <footer>
    <div class="small-12 large-12 columns">
      <div class="copyrights">
        <p>&copy; Copyright 2017 Jakov Ivankovic</p>
      </div>
    </div>
  </footer>
</div>


<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/dist/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="js/app.js"></script>
</body>
